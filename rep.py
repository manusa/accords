#!/usr/bin/env python

import os
import re
import numpy as np
import matplotlib.pyplot as plt     
from collections import OrderedDict

def plot_accord(acc, ax = None):
    if ax is None:
        ax = plt.subplot(111, polar=True)

    # Set the circumference labels
    ax.set_xticks(np.linspace(0, 2*np.pi, 13))
 #  ax.set_xticklabels(list(np.array(range(12))-5))
    ax.set_xticklabels(['F', 'C', 'G', 'D', 'A', 'E', 'B', 'F#', 'C#', 'G#', 'D#', 'A#'], fontsize=3)
    ax.set_rticks([])

    # Make the labels go clockwise
    ax.set_theta_direction(-1)       

    # Place 0 at the top
   #ax.set_theta_zero_location('N')
  # ax.set_theta_offset(np.pi/12.)
    ax.set_theta_offset(-5.*np.pi/6.0+np.pi/12.)

    couleur = ['r', 'b', 'g', 'k', 'k']
    for iin, n in enumerate(acc):
        ax.plot((np.pi*(4./6.+n/6.))%(2.*np.pi), 0.95, color=couleur[iin], marker='o')


# accords:

accords = OrderedDict([
   ('majeur',   (0, 1,  4)),
   ('majeur7',  (0, 1,  4, -2)),
   ('majeur6',  (0, 1,  4, 3)),
   ('majeur7M', (0, 1,  4, 5)),
   ('mineur',   (0, 1, -3)),
   ('mineur6',  (0, 1, -3, 3)),
   ('mineur7',  (0, 1, -3, -2)),
   ('mineurAdd13-', (0, 1, -3, -4)),
   ('majeur79', (0, 1, 4, -2, 2)),
   ('majeur69', (0, 1, 4, 3, 2)),
   ('majeur7aug5', (0, 1, 4, -4)),
   ('demiDim',  (0,-3, -2, 6)),
   ('mineur6Add11', (0, 1,  -3, -1, 3)),
   ('mineur7Add11', (0, 1,  -3, -1, -2)),
   ('diminue',  (0, -3, 3 , 6)),
   ('augmente', (0, 4, -4))
])

# for acc in majeur, mineur, mineur6, demiDim, diminue, majeur7M, majeur69, majeur7aug5:
#      plot_accord(acc)

# plt.show()

