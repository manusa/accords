#!/usr/bin/env python

import os
import re
import docutils
from docutils.core import publish_string, publish_doctree
import matplotlib
from pylatex import Document, Section, Subsection, Tabular, MultiColumn,\
        MultiRow, Figure, NoEscape, NewPage

# matplotlib.use('Agg')  # Not to use X server. For TravisCI.
import matplotlib.pyplot as plt

from copy import copy

from rep import *

 #----------------------------------------------------------------------------

def unique(se):
    seq=copy(se)
    seen = {}
    pos = 0
    for item in seq:
        if item not in seen:
            seen[item] = True
            seq[pos] = item
            pos += 1
    del seq[pos:]
    return seq


class carre(object):
    def __init(self,s):
        self.acc = []
        for i in s.count('^'):
            continue
            # on verra


class Tab(object):
    def __init__(self, s):
        self.titre = re.search(r'.* :', s, re.M).group(0)[:-2]
        self.tonalite = re.search(r'\(.*\)', s, re.M).group(0).replace('(','').replace(')','')
        self.structure = re.search(r'\).*$', s, re.M).group(0)[1:].replace(' ','').split('-')
        self.accords = self.__extrait_accords(s)

    def __extrait_accords(self, s):
        print('extract accord')
        s = re.split(r'(?m)^\**$', s)[1]
        accords = []
        for struct in unique(self.structure):
            for desacc in re.split('(?m)\n\n', s):
                if desacc.isspace() or len(desacc)==0:
                    continue
                astruc = []
                for ll in desacc.split('- '):
                    for lll in ll.split('\n'):
                        lll = lll.replace(' ', '')
                        if lll == '':
                            lll='%'
                        astruc.append(lll)
                print("astruct: ", astruc)
                accords.append(astruc)
        return accords

    def __str__(self):
        s = 'Morceau: %s\n tonalite: %s\n structure: %s\n  accords\
        \n--------------\n'\
                %(self.titre, self.tonalite, ' '.join(self.structure))
        for struct, sacc in zip(unique(self.structure), self.accords):
            s += struct + ' : '
            for il in sacc:
                if il.isspace() or len(il)==0:
                    continue
                for i  in il.split('- '):
                    s += ' |\t' + i + '\t'
            s += ' |\n'
        s += '-------------------\n'
#       return s.encode("utf8")
        return s



class Tabs(object):
    def __init__(self, filename):
        self.filename = filename
        self.lire_tabs()
        self.tabs = []
        self.lire_tabs()

    def lire_tabs(self):
        with open(self.filename) as f:
            self.tabs = []
            une_tab = ''
            for line in f:
                # ----------------------
                if line.isspace() and p_line_vide or line[0] =='#':
                    p_line_vide = True
                elif line.count('-') >= 34:
                    self.tabs.append(Tab(une_tab))
                    une_tab = ''
                    p_line_vide = True
                else:
                    une_tab += line
                    p_line_vide = False

    def affiche(self):
        print(self)

    def __str__(self):
        s = ""
        for tab in self.tabs:
            s += str(tab) + '\n'
       #return s.encode("utf8")
        return s

 #----------------------------------------------------------------------------


def main(tabname, fname, width=600, *args, **kwargs):
    geometry_options = {"right": "2cm", "left": "2cm"}
    doc = Document(fname, geometry_options=geometry_options)

    doc.append('Introduction.')

    # les accords
    iacc = 1
    for nom_acc, acc in accords.items():
        ax = plt.subplot(4,4, iacc, polar=True)
        plt.subplots_adjust(wspace=0.0, hspace=0.5)
        plot_accord(acc, ax)
        ax.set_title(nom_acc,fontsize=8)
        iacc += 1

    with doc.create(Figure(position='htbp')) as plot:
        plot.add_plot(width='18cm', *args, **kwargs)
        plot.add_caption('les accords.')

    doc.append(NewPage())

    # les morceaux
    with doc.create(Section('Morceaux')):
        tabs = Tabs(tabname)
        for tab in tabs.tabs:
            doc.append(NoEscape((str(tab.titre)+ ' en ' + (str(tab.tonalite)))) + ' :    [ ')
            doc.append(NoEscape(str(' '.join(tab.structure)))+'  ]\n')

            nbe_acc = len(tab.accords[0])
           #print(tab.titre)
           #print(nbe_acc)
           #print(tab.accords)
            maxc=8 # if nbe_acc % 8 == 0 else 200
            table1 = Tabular('|c'*min(nbe_acc, maxc)+'|c|')
            table1.add_hline()

            for struct, sacc in zip(unique(tab.structure), tab.accords):
                s = [struct]
                print("sacc = ", sacc)
                for il in sacc:
           #        print("il = ", il)
                    s.append(il)
           #        print("s = ", s)
                    # si besoin d'une autre ligne
                    if len(s) == maxc+1:
                        table1.add_row(tuple(s))
                        s=['']

           #    print("s = ", s)
                print("len s = ", len(s))
                if len(s) is not 1:
                    # pad jsuqu'a la fin de la ligne
                    while len(s) < min(nbe_acc, maxc) +1:
                        s.append('')
                        print("len s = ", len(s))
                    table1.add_row(tuple(s))
                table1.add_hline()

            doc.append(table1)
            doc.append('\n\n\n')

    doc.append('Conclusion.')

    doc.generate_pdf(clean_tex=False)

    tabs.affiche()


if __name__ == '__main__':
    x = [0, 1, 2, 3, 4, 5, 6]
    y = [15, 2, 7, 1, 5, 6, 9]

#    plt.plot(x, y)

    main('tabs.rst', 'accords', r'1\textwidth', dpi=300)
    # main('tabs.rst', 'matplotlib_ex-facecolor', r'0.5\textwidth', facecolor='b')
    #
