#!/usr/bin/env python

import os
import re

import numpy as np
import matplotlib.pyplot as plt

# notes
notes=['A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#']

f=np.zeros((2,6))
f[1,:]=[1, 3/2., 5/4., 7/4., 9/8., 11/8.]

fig, ax = plt.subplots()
ax.set_xscale('symlog')
ax.set_xlabel('freq')
ax.grid()
ax.xaxis.grid(which='minor')  # minor grid on too
ax.scatter(f[1,:], f[0,:])

for i, l in enumerate([1, 3, 5, 7, 9, 11]):
    ax.annotate(l, (f[1,i], f[0,i]))

ft=np.ones((2,12))
r=2**(1/12.)
ft[1,:]=[r**i for i in range(12)]

ax.scatter(ft[1,:], ft[0,:])

for i, l in enumerate(notes):
    ax.annotate(i, (ft[1,i], ft[0,i]))
    ax.annotate(l, (ft[1,i], 0.5))

print("f=", f)
print("ft=", ft)

# plt.semilogx(f[1,:], f[0,:],'o')
# plt.semilogx(ft[1,:], ft[0,:],'x')

plt.show()



r = np.arange(0, 2, 0.01)
theta = 2 * np.pi * r

fig, ax = plt.subplots(subplot_kw={'projection': 'polar'})
ax.plot(theta, r)
ax.set_rmax(2)
ax.set_rticks([0.5, 1, 1.5, 2])  # Less radial ticks
ax.set_rlabel_position(-22.5)  # Move radial labels away from plotted line
ax.grid(True)

ax.set_title("A line plot on a polar axis", va='bottom')
plt.show()
